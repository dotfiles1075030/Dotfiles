#!/bin/bash

# This script will randomly go through the files of a directory, setting it
# up as the wallpaper at regular intervals
#
# NOTE: this script is in bash (not posix shell), because the RANDOM variable
# we use is not defined in posix
swww img --transition-type any --transition-fps 60 --transition-duration 4 `find ~/Documentos/Sync-PCs/Wallpapers/16:9 -type f | shuf -n 1`
