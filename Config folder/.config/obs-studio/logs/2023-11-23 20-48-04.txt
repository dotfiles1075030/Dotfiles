20:48:04.778: Using EGL/X11
20:48:04.779: CPU Name: AMD Ryzen 7 4800HS with Radeon Graphics
20:48:04.779: CPU Speed: 4292.277MHz
20:48:04.779: Physical Cores: 8, Logical Cores: 16
20:48:04.779: Physical Memory: 15405MB Total, 6254MB Free
20:48:04.779: Kernel Version: Linux 6.6.2-zen1-1-zen
20:48:04.779: Distribution: "Arch Linux" Unknown
20:48:04.779: Desktop Environment: Hyprland (hyprland)
20:48:04.779: Session Type: wayland
20:48:04.779: Window System: X11.0, Vendor: The X.Org Foundation, Version: 1.23.2
20:48:04.782: Qt Version: 6.6.0 (runtime), 6.6.0 (compiled)
20:48:04.782: Portable mode: false
20:48:04.912: OBS 30.0.0-1 (linux)
20:48:04.912: ---------------------------------
20:48:04.912: ---------------------------------
20:48:04.912: audio settings reset:
20:48:04.912: 	samples per sec: 48000
20:48:04.912: 	speakers:        2
20:48:04.912: 	max buffering:   960 milliseconds
20:48:04.912: 	buffering type:  dynamically increasing
20:48:04.917: ---------------------------------
20:48:04.917: Initializing OpenGL...
20:48:04.995: Loading up OpenGL on adapter AMD AMD Radeon Graphics (renoir, LLVM 16.0.6, DRM 3.54, 6.6.2-zen1-1-zen)
20:48:04.995: OpenGL loaded successfully, version 4.6 (Core Profile) Mesa 23.2.1-arch1.2, shading language 4.60
20:48:05.028: ---------------------------------
20:48:05.028: video settings reset:
20:48:05.028: 	base resolution:   1920x1080
20:48:05.028: 	output resolution: 1280x720
20:48:05.028: 	downscale filter:  Bicubic
20:48:05.028: 	fps:               60/1
20:48:05.028: 	format:            NV12
20:48:05.028: 	YUV mode:          Rec. 709/Partial
20:48:05.029: NV12 texture support not available
20:48:05.029: P010 texture support not available
20:48:05.035: Audio monitoring device:
20:48:05.035: 	name: Padrão
20:48:05.035: 	id: default
20:48:05.035: ---------------------------------
20:48:05.051: Failed to load 'en-US' text for module: 'decklink-captions.so'
20:48:05.068: Failed to load 'en-US' text for module: 'decklink-output-ui.so'
20:48:05.083: A DeckLink iterator could not be created.  The DeckLink drivers may not be installed
20:48:05.083: Failed to initialize module 'decklink.so'
20:48:05.344: [pipewire] No captures available
20:48:05.385: v4l2loopback not installed, virtual camera disabled
20:48:05.447: VAAPI: Failed to initialize display in vaapi_device_h264_supported
20:48:05.448: FFmpeg VAAPI H264 encoding not supported
20:48:05.448: VAAPI: Failed to initialize display in vaapi_device_hevc_supported
20:48:05.448: FFmpeg VAAPI HEVC encoding not supported
20:48:05.635: ---------------------------------
20:48:05.635:   Loaded Modules:
20:48:05.635:     wlrobs.so
20:48:05.635:     text-freetype2.so
20:48:05.635:     rtmp-services.so
20:48:05.635:     obs-x264.so
20:48:05.635:     obs-vst.so
20:48:05.635:     obs-transitions.so
20:48:05.635:     obs-qsv11.so
20:48:05.635:     obs-outputs.so
20:48:05.635:     obs-libfdk.so
20:48:05.635:     obs-filters.so
20:48:05.635:     obs-ffmpeg.so
20:48:05.635:     linux-v4l2.so
20:48:05.635:     linux-pulseaudio.so
20:48:05.635:     linux-pipewire.so
20:48:05.635:     linux-jack.so
20:48:05.635:     linux-capture.so
20:48:05.635:     linux-alsa.so
20:48:05.635:     image-source.so
20:48:05.635:     frontend-tools.so
20:48:05.635:     decklink-output-ui.so
20:48:05.635:     decklink-captions.so
20:48:05.635: ---------------------------------
20:48:05.635: ==== Startup complete ===============================================
20:48:05.660: All scene data cleared
20:48:05.660: ------------------------------------------------
20:48:05.666: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.85) 15.0.0'
20:48:05.667: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
20:48:05.667: pulse-input: Started recording from 'alsa_output.pci-0000_04_00.6.analog-stereo.monitor' (default)
20:48:05.667: [Loaded global audio device]: 'Áudio do desktop'
20:48:05.668: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.85) 15.0.0'
20:48:05.668: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
20:48:05.669: pulse-input: Started recording from 'alsa_input.pci-0000_04_00.6.analog-stereo' (default)
20:48:05.669: [Loaded global audio device]: 'Mic/Aux'
20:48:05.669: xshm-input: Geometry 1920x1080 @ 0,0
20:48:05.673: Switched to scene 'Cena'
20:48:05.673: ------------------------------------------------
20:48:05.673: Loaded scenes:
20:48:05.673: - scene 'Cena':
20:48:05.673:     - source: 'Captura de Tela (XSHM)' (xshm_input)
20:48:05.673: ------------------------------------------------
20:48:06.321: adding 21 milliseconds of audio buffering, total audio buffering is now 21 milliseconds (source: Áudio do desktop)
20:48:06.321: 
20:48:16.187: xshm-input: Geometry 1920x1080 @ 0,0
20:48:20.977: User Removed source 'Captura de Tela (XSHM)' (xshm_input) from scene 'Cena'
20:48:35.935: User added source 'Wayland output(scpy)' (wlrobs-scpy) to scene 'Cena'
20:50:17.576: Source 'Cena' renamed to 'Screen Capture'
20:50:23.481: User added scene 'Window Capture'
20:50:23.511: User switched to scene 'Window Capture'
20:50:36.358: User switched to scene 'Screen Capture'
20:50:37.374: User switched to scene 'Window Capture'
20:50:45.237: User added source 'Wayland output(dmabuf)' (wlrobs-dmabuf) to scene 'Window Capture'
20:50:55.643: User Removed source 'Wayland output(dmabuf)' (wlrobs-dmabuf) from scene 'Window Capture'
20:50:59.033: User switched to scene 'Screen Capture'
20:51:00.122: User switched to scene 'Window Capture'
20:51:17.908: [window-capture: 'Captura de janela (Xcomposite)'] update settings:
20:51:17.908: 	title: Criar ou reutilizar fonte
20:51:17.908: 	class: obs
20:51:17.908: 
20:51:17.911: User added source 'Captura de janela (Xcomposite)' (xcomposite_input) to scene 'Window Capture'
20:51:17.936: [window-capture: 'Captura de janela (Xcomposite)'] update settings:
20:51:17.936: 	title: OBS 30.0.0-1 - Perfil: Sem nome - Cenas: Sem nome
20:51:17.936: 	class: obs
20:51:17.936: 
20:51:20.027: X Error: BadWindow (invalid Window parameter), Major opcode: BadValue (integer parameter out of range for operation), Minor opcode: 0, Serial: 438
20:51:25.935: [window-capture: 'Captura de janela (Xcomposite)'] update settings:
20:51:25.935: 	title: OBS 30.0.0-1 - Perfil: Sem nome - Cenas: Sem nome
20:51:25.935: 	class: obs
20:51:25.935: 
20:51:47.778: User Removed source 'Captura de janela (Xcomposite)' (xcomposite_input) from scene 'Window Capture'
20:52:04.574: [window-capture: 'Captura de janela (Xcomposite)'] update settings:
20:52:04.574: 	title: Criar ou reutilizar fonte
20:52:04.574: 	class: obs
20:52:04.574: 
20:52:04.576: User added source 'Captura de janela (Xcomposite)' (xcomposite_input) to scene 'Window Capture'
20:52:04.585: [window-capture: 'Captura de janela (Xcomposite)'] update settings:
20:52:04.585: 	title: OBS 30.0.0-1 - Perfil: Sem nome - Cenas: Sem nome
20:52:04.585: 	class: obs
20:52:04.585: 
20:52:12.202: X Error: BadWindow (invalid Window parameter), Major opcode: BadValue (integer parameter out of range for operation), Minor opcode: 0, Serial: 7126
20:52:20.117: Saved screenshot to '/home/fleurc/Screenshot 2023-11-23 20-52-20.png'
20:52:31.301: [window-capture: 'Captura de janela (Xcomposite)'] update settings:
20:52:31.301: 	title: OBS 30.0.0-1 - Perfil: Sem nome - Cenas: Sem nome
20:52:31.301: 	class: obs
20:52:31.301: 
20:52:36.348: User Removed source 'Captura de janela (Xcomposite)' (xcomposite_input) from scene 'Window Capture'
20:52:41.726: User switched to scene 'Screen Capture'
20:52:41.729: User Removed scene 'Window Capture'
21:02:42.628: User added scene 'Window Capture'
21:02:42.630: User switched to scene 'Window Capture'
21:02:43.705: ==== Shutting down ==================================================
21:02:43.722: pulse-input: Stopped recording from 'alsa_output.pci-0000_04_00.6.analog-stereo.monitor'
21:02:43.722: pulse-input: Got 35117 packets with 42140400 frames
21:02:43.722: pulse-input: Stopped recording from 'alsa_input.pci-0000_04_00.6.analog-stereo'
21:02:43.722: pulse-input: Got 35117 packets with 42140400 frames
21:02:43.741: All scene data cleared
21:02:43.741: ------------------------------------------------
21:02:43.816: [Scripting] Total detached callbacks: 0
21:02:43.817: Freeing OBS context data
21:02:43.845: == Profiler Results =============================
21:02:43.845: run_program_init: 1089.15 ms
21:02:43.845:  ┣OBSApp::AppInit: 7.985 ms
21:02:43.845:  ┃ ┗OBSApp::InitLocale: 3.643 ms
21:02:43.845:  ┗OBSApp::OBSInit: 992.699 ms
21:02:43.845:    ┣obs_startup: 3.356 ms
21:02:43.845:    ┗OBSBasic::OBSInit: 859.29 ms
21:02:43.846:      ┣OBSBasic::InitBasicConfig: 0.159 ms
21:02:43.846:      ┣OBSBasic::ResetAudio: 0.78 ms
21:02:43.846:      ┣OBSBasic::ResetVideo: 121.689 ms
21:02:43.846:      ┃ ┗obs_init_graphics: 115.485 ms
21:02:43.846:      ┃   ┗shader compilation: 33.446 ms
21:02:43.846:      ┣OBSBasic::InitOBSCallbacks: 0.011 ms
21:02:43.846:      ┣OBSBasic::InitHotkeys: 0.057 ms
21:02:43.846:      ┣obs_load_all_modules2: 600.005 ms
21:02:43.846:      ┃ ┣obs_init_module(decklink-captions.so): 0.001 ms
21:02:43.846:      ┃ ┣obs_init_module(decklink-output-ui.so): 0.002 ms
21:02:43.846:      ┃ ┣obs_init_module(decklink.so): 0.221 ms
21:02:43.846:      ┃ ┣obs_init_module(frontend-tools.so): 123.12 ms
21:02:43.846:      ┃ ┣obs_init_module(image-source.so): 0.024 ms
21:02:43.846:      ┃ ┣obs_init_module(linux-alsa.so): 0.006 ms
21:02:43.846:      ┃ ┣obs_init_module(linux-capture.so): 0.87 ms
21:02:43.846:      ┃ ┣obs_init_module(linux-jack.so): 0.005 ms
21:02:43.846:      ┃ ┣obs_init_module(linux-pipewire.so): 19.743 ms
21:02:43.846:      ┃ ┣obs_init_module(linux-pulseaudio.so): 0.012 ms
21:02:43.846:      ┃ ┣obs_init_module(linux-v4l2.so): 7.67 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-ffmpeg.so): 1.414 ms
21:02:43.846:      ┃ ┃ ┗nvenc_check: 0.839 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-filters.so): 0.038 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-libfdk.so): 0.006 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-outputs.so): 0.008 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-qsv11.so): 0.228 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-transitions.so): 0.013 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-vst.so): 0.011 ms
21:02:43.846:      ┃ ┣obs_init_module(obs-x264.so): 0.007 ms
21:02:43.846:      ┃ ┣obs_init_module(rtmp-services.so): 1.212 ms
21:02:43.846:      ┃ ┣obs_init_module(text-freetype2.so): 0.038 ms
21:02:43.846:      ┃ ┗obs_init_module(wlrobs.so): 0.034 ms
21:02:43.846:      ┣OBSBasic::InitService: 1.555 ms
21:02:43.846:      ┣OBSBasic::ResetOutputs: 0.169 ms
21:02:43.846:      ┣OBSBasic::CreateHotkeys: 0.029 ms
21:02:43.846:      ┣OBSBasic::InitPrimitives: 0.102 ms
21:02:43.846:      ┗OBSBasic::Load: 35.152 ms
21:02:43.846: obs_hotkey_thread(25 ms): min=0.081 ms, median=0.245 ms, max=6.214 ms, 99th percentile=0.739 ms, 100% below 25 ms
21:02:43.846: audio_thread(Audio): min=0.012 ms, median=5.944 ms, max=50.162 ms, 99th percentile=30.982 ms
21:02:43.846: obs_graphics_thread(16.6667 ms): min=0.287 ms, median=16.86 ms, max=72.289 ms, 99th percentile=35.952 ms, 46.3429% below 16.667 ms
21:02:43.846:  ┣tick_sources: min=0.002 ms, median=0.01 ms, max=32.53 ms, 99th percentile=2.419 ms
21:02:43.846:  ┣output_frame: min=0.11 ms, median=16.056 ms, max=57.412 ms, 99th percentile=34.402 ms
21:02:43.846:  ┃ ┗gs_context(video->graphics): min=0.108 ms, median=16.053 ms, max=57.409 ms, 99th percentile=34.399 ms
21:02:43.846:  ┃   ┣render_video: min=0.012 ms, median=15.837 ms, max=57.161 ms, 99th percentile=34.164 ms
21:02:43.846:  ┃   ┃ ┗render_main_texture: min=0.009 ms, median=15.83 ms, max=57.152 ms, 99th percentile=34.155 ms
21:02:43.846:  ┃   ┗gs_flush: min=0.002 ms, median=0.188 ms, max=1.287 ms, 99th percentile=0.412 ms
21:02:43.846:  ┗render_displays: min=0.006 ms, median=0.781 ms, max=42.124 ms, 99th percentile=17.33 ms
21:02:43.846: =================================================
21:02:43.846: == Profiler Time Between Calls ==================
21:02:43.846: obs_hotkey_thread(25 ms): min=25.125 ms, median=25.34 ms, max=31.312 ms, 85.5633% within ±2% of 25 ms (0% lower, 14.4367% higher)
21:02:43.847: obs_graphics_thread(16.6667 ms): min=4.348 ms, median=16.931 ms, max=72.304 ms, 35.9424% within ±2% of 16.667 ms (15.65% lower, 48.4076% higher)
21:02:43.847: =================================================
21:02:43.941: Number of memory leaks: 2
