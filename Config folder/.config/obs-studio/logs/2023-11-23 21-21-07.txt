21:21:07.382: Platform: Wayland
21:21:07.382: CPU Name: AMD Ryzen 7 4800HS with Radeon Graphics
21:21:07.382: CPU Speed: 3941.637MHz
21:21:07.383: Physical Cores: 8, Logical Cores: 16
21:21:07.383: Physical Memory: 15405MB Total, 13004MB Free
21:21:07.383: Kernel Version: Linux 6.6.2-zen1-1-zen
21:21:07.383: Distribution: "Arch Linux" Unknown
21:21:07.383: Desktop Environment: Hyprland (hyprland)
21:21:07.383: Session Type: wayland
21:21:07.386: Qt Version: 6.6.0 (runtime), 6.6.0 (compiled)
21:21:07.386: Portable mode: false
21:21:07.490: OBS 30.0.0-1 (linux)
21:21:07.490: ---------------------------------
21:21:07.491: ---------------------------------
21:21:07.491: audio settings reset:
21:21:07.491: 	samples per sec: 48000
21:21:07.491: 	speakers:        2
21:21:07.491: 	max buffering:   960 milliseconds
21:21:07.491: 	buffering type:  dynamically increasing
21:21:07.494: ---------------------------------
21:21:07.494: Initializing OpenGL...
21:21:07.494: Using EGL/Wayland
21:21:07.544: Initialized EGL 1.5
21:21:07.568: Loading up OpenGL on adapter AMD AMD Radeon Graphics (renoir, LLVM 16.0.6, DRM 3.54, 6.6.2-zen1-1-zen)
21:21:07.568: OpenGL loaded successfully, version 4.6 (Core Profile) Mesa 23.2.1-arch1.2, shading language 4.60
21:21:07.601: ---------------------------------
21:21:07.601: video settings reset:
21:21:07.601: 	base resolution:   1920x1080
21:21:07.601: 	output resolution: 1280x720
21:21:07.601: 	downscale filter:  Bicubic
21:21:07.601: 	fps:               60/1
21:21:07.601: 	format:            NV12
21:21:07.601: 	YUV mode:          Rec. 709/Partial
21:21:07.601: NV12 texture support not available
21:21:07.601: P010 texture support not available
21:21:07.607: Audio monitoring device:
21:21:07.607: 	name: Padrão
21:21:07.607: 	id: default
21:21:07.607: ---------------------------------
21:21:07.620: Failed to load 'en-US' text for module: 'decklink-captions.so'
21:21:07.631: Failed to load 'en-US' text for module: 'decklink-output-ui.so'
21:21:07.642: A DeckLink iterator could not be created.  The DeckLink drivers may not be installed
21:21:07.642: Failed to initialize module 'decklink.so'
21:21:07.829: [pipewire] Available captures:
21:21:07.829: [pipewire]     - Desktop capture
21:21:07.829: [pipewire]     - Window capture
21:21:07.860: v4l2loopback not installed, virtual camera disabled
21:21:07.899: VAAPI: Failed to initialize display in vaapi_device_h264_supported
21:21:07.900: FFmpeg VAAPI H264 encoding not supported
21:21:07.900: VAAPI: Failed to initialize display in vaapi_device_hevc_supported
21:21:07.900: FFmpeg VAAPI HEVC encoding not supported
21:21:08.042: ---------------------------------
21:21:08.042:   Loaded Modules:
21:21:08.042:     wlrobs.so
21:21:08.042:     text-freetype2.so
21:21:08.042:     rtmp-services.so
21:21:08.042:     obs-x264.so
21:21:08.042:     obs-vst.so
21:21:08.042:     obs-transitions.so
21:21:08.042:     obs-qsv11.so
21:21:08.042:     obs-outputs.so
21:21:08.042:     obs-libfdk.so
21:21:08.042:     obs-filters.so
21:21:08.042:     obs-ffmpeg.so
21:21:08.042:     linux-v4l2.so
21:21:08.042:     linux-pulseaudio.so
21:21:08.042:     linux-pipewire.so
21:21:08.042:     linux-jack.so
21:21:08.042:     linux-capture.so
21:21:08.042:     linux-alsa.so
21:21:08.042:     image-source.so
21:21:08.042:     frontend-tools.so
21:21:08.042:     decklink-output-ui.so
21:21:08.042:     decklink-captions.so
21:21:08.042: ---------------------------------
21:21:08.042: ==== Startup complete ===============================================
21:21:08.071: All scene data cleared
21:21:08.071: ------------------------------------------------
21:21:08.075: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.85) 15.0.0'
21:21:08.076: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
21:21:08.076: pulse-input: Started recording from 'alsa_output.pci-0000_04_00.6.analog-stereo.monitor' (default)
21:21:08.076: [Loaded global audio device]: 'Áudio do desktop'
21:21:08.077: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.85) 15.0.0'
21:21:08.077: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
21:21:08.077: pulse-input: Started recording from 'alsa_input.pci-0000_04_00.6.analog-stereo' (default)
21:21:08.077: [Loaded global audio device]: 'Mic/Aux'
21:21:08.079: Switched to scene 'Window Capture'
21:21:08.079: ------------------------------------------------
21:21:08.079: Loaded scenes:
21:21:08.079: - scene 'Screen Capture':
21:21:08.079:     - source: 'Wayland output(scpy)' (wlrobs-scpy)
21:21:08.079: - scene 'Window Capture':
21:21:08.079: ------------------------------------------------
21:21:08.643: adding 21 milliseconds of audio buffering, total audio buffering is now 21 milliseconds (source: Áudio do desktop)
21:21:08.643: 
21:21:17.257: PipeWire initialized
21:21:17.260: User added source 'Captura de Janela (PipeWire)' (pipewire-window-capture-source) to scene 'Window Capture'
21:21:17.270: [pipewire] Screencast session created
21:21:21.808: [pipewire] Asking for window
21:21:21.812: [pipewire] window selected, setting up screencast
21:21:21.818: [pipewire] Server version: 0.3.85
21:21:21.818: [pipewire] Library version: 0.3.85
21:21:21.818: [pipewire] Header version: 0.3.84
21:21:21.818: [pipewire] Created stream 0x564cce88c3b0
21:21:21.818: [pipewire] Stream 0x564cce88c3b0 state: "connecting" (error: none)
21:21:21.818: [pipewire] Playing stream 0x564cce88c3b0
21:21:21.819: [pipewire] Stream 0x564cce88c3b0 state: "paused" (error: none)
21:21:21.823: [pipewire] Negotiated format:
21:21:21.823: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.824: [pipewire]     Modifier: 0x0
21:21:21.824: [pipewire]     Size: 1874x985
21:21:21.824: [pipewire]     Framerate: 0/1
21:21:21.825: [pipewire] Negotiated format:
21:21:21.825: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.825: [pipewire]     Modifier: 0x20000044051ba01
21:21:21.825: [pipewire]     Size: 1874x985
21:21:21.825: [pipewire]     Framerate: 0/1
21:21:21.846: [pipewire] Stream 0x564cce88c3b0 state: "streaming" (error: none)
21:21:21.855: Cannot create EGLImage: Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).
21:21:21.855: [pipewire] Renegotiating stream
21:21:21.855: [pipewire] Negotiated format:
21:21:21.856: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.856: [pipewire]     Modifier: 0x20000044051ba01
21:21:21.856: [pipewire]     Size: 1874x985
21:21:21.856: [pipewire]     Framerate: 0/1
21:21:21.857: [pipewire] Negotiated format:
21:21:21.857: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.857: [pipewire]     Modifier: 0x20000044051b901
21:21:21.857: [pipewire]     Size: 1874x985
21:21:21.857: [pipewire]     Framerate: 0/1
21:21:21.894: Cannot create EGLImage: Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).
21:21:21.894: [pipewire] Renegotiating stream
21:21:21.895: [pipewire] Stream 0x564cce88c3b0 state: "paused" (error: none)
21:21:21.896: [pipewire] Negotiated format:
21:21:21.896: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.896: [pipewire]     Modifier: 0x20000044051b901
21:21:21.896: [pipewire]     Size: 1874x985
21:21:21.896: [pipewire]     Framerate: 0/1
21:21:21.896: [pipewire] Negotiated format:
21:21:21.896: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.896: [pipewire]     Modifier: 0x200000440517901
21:21:21.896: [pipewire]     Size: 1874x985
21:21:21.896: [pipewire]     Framerate: 0/1
21:21:21.898: [pipewire] Stream 0x564cce88c3b0 state: "streaming" (error: none)
21:21:21.922: Cannot create EGLImage: Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).
21:21:21.922: [pipewire] Renegotiating stream
21:21:21.922: [pipewire] Negotiated format:
21:21:21.922: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.922: [pipewire]     Modifier: 0x200000440517901
21:21:21.922: [pipewire]     Size: 1874x985
21:21:21.922: [pipewire]     Framerate: 0/1
21:21:21.923: [pipewire] Negotiated format:
21:21:21.923: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:21.923: [pipewire]     Modifier: 0x200000000401a01
21:21:21.923: [pipewire]     Size: 1874x985
21:21:21.923: [pipewire]     Framerate: 0/1
21:21:24.572: User switched to scene 'Screen Capture'
21:21:24.917: [pipewire] Stream 0x564cce88c3b0 state: "paused" (error: none)
21:21:27.832: User Removed source 'Wayland output(scpy)' (wlrobs-scpy) from scene 'Screen Capture'
21:21:36.990: PipeWire initialized
21:21:36.992: User added source 'Captura de tela (PipeWire)' (pipewire-desktop-capture-source) to scene 'Screen Capture'
21:21:37.002: [pipewire] Screencast session created
21:21:38.585: [pipewire] Asking for desktop
21:21:38.589: [pipewire] desktop selected, setting up screencast
21:21:38.592: [pipewire] Server version: 0.3.85
21:21:38.593: [pipewire] Library version: 0.3.85
21:21:38.593: [pipewire] Header version: 0.3.84
21:21:38.593: [pipewire] Created stream 0x564cce857780
21:21:38.593: [pipewire] Stream 0x564cce857780 state: "connecting" (error: none)
21:21:38.593: [pipewire] Playing stream 0x564cce857780
21:21:38.593: [pipewire] Stream 0x564cce857780 state: "paused" (error: none)
21:21:38.597: [pipewire] Negotiated format:
21:21:38.597: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.597: [pipewire]     Modifier: 0x0
21:21:38.597: [pipewire]     Size: 1920x1080
21:21:38.597: [pipewire]     Framerate: 0/1
21:21:38.597: [pipewire] Negotiated format:
21:21:38.597: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.597: [pipewire]     Modifier: 0x20000044051ba01
21:21:38.597: [pipewire]     Size: 1920x1080
21:21:38.597: [pipewire]     Framerate: 0/1
21:21:38.599: [pipewire] Stream 0x564cce857780 state: "streaming" (error: none)
21:21:38.609: Cannot create EGLImage: Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).
21:21:38.609: [pipewire] Renegotiating stream
21:21:38.610: [pipewire] Negotiated format:
21:21:38.610: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.610: [pipewire]     Modifier: 0x20000044051ba01
21:21:38.610: [pipewire]     Size: 1920x1080
21:21:38.610: [pipewire]     Framerate: 0/1
21:21:38.611: [pipewire] Negotiated format:
21:21:38.611: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.611: [pipewire]     Modifier: 0x20000044051b901
21:21:38.611: [pipewire]     Size: 1920x1080
21:21:38.611: [pipewire]     Framerate: 0/1
21:21:38.642: Cannot create EGLImage: Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).
21:21:38.642: [pipewire] Renegotiating stream
21:21:38.643: [pipewire] Stream 0x564cce857780 state: "paused" (error: none)
21:21:38.643: [pipewire] Negotiated format:
21:21:38.643: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.643: [pipewire]     Modifier: 0x20000044051b901
21:21:38.643: [pipewire]     Size: 1920x1080
21:21:38.643: [pipewire]     Framerate: 0/1
21:21:38.644: [pipewire] Negotiated format:
21:21:38.644: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.644: [pipewire]     Modifier: 0x200000440517901
21:21:38.644: [pipewire]     Size: 1920x1080
21:21:38.644: [pipewire]     Framerate: 0/1
21:21:38.661: [pipewire] Stream 0x564cce857780 state: "streaming" (error: none)
21:21:38.675: Cannot create EGLImage: Arguments are inconsistent (for example, a valid context requires buffers not supplied by a valid surface).
21:21:38.675: [pipewire] Renegotiating stream
21:21:38.675: [pipewire] Negotiated format:
21:21:38.675: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.675: [pipewire]     Modifier: 0x200000440517901
21:21:38.676: [pipewire]     Size: 1920x1080
21:21:38.676: [pipewire]     Framerate: 0/1
21:21:38.676: [pipewire] Negotiated format:
21:21:38.676: [pipewire]     Format: 8 (Spa:Enum:VideoFormat:BGRx)
21:21:38.676: [pipewire]     Modifier: 0x200000000401a01
21:21:38.676: [pipewire]     Size: 1920x1080
21:21:38.676: [pipewire]     Framerate: 0/1
21:21:41.665: User switched to scene 'Window Capture'
21:21:41.680: [pipewire] Stream 0x564cce88c3b0 state: "streaming" (error: none)
21:21:42.013: [pipewire] Stream 0x564cce857780 state: "paused" (error: none)
21:22:12.546: ==== Shutting down ==================================================
21:22:12.550: pulse-input: Stopped recording from 'alsa_output.pci-0000_04_00.6.analog-stereo.monitor'
21:22:12.550: pulse-input: Got 2557 packets with 3068400 frames
21:22:12.550: pulse-input: Stopped recording from 'alsa_input.pci-0000_04_00.6.analog-stereo'
21:22:12.550: pulse-input: Got 2556 packets with 3067200 frames
21:22:12.551: [pipewire] Stream 0x564cce857780 state: "unconnected" (error: none)
21:22:12.552: [pipewire] Stream 0x564cce88c3b0 state: "paused" (error: none)
21:22:12.552: [pipewire] Stream 0x564cce88c3b0 state: "unconnected" (error: none)
21:22:12.569: All scene data cleared
21:22:12.569: ------------------------------------------------
21:22:12.617: [Scripting] Total detached callbacks: 0
21:22:12.617: Freeing OBS context data
21:22:12.632: == Profiler Results =============================
21:22:12.632: run_program_init: 837.42 ms
21:22:12.632:  ┣OBSApp::AppInit: 8.745 ms
21:22:12.632:  ┃ ┗OBSApp::InitLocale: 4.029 ms
21:22:12.632:  ┗OBSApp::OBSInit: 757.244 ms
21:22:12.632:    ┣obs_startup: 3.778 ms
21:22:12.632:    ┗OBSBasic::OBSInit: 648.711 ms
21:22:12.632:      ┣OBSBasic::InitBasicConfig: 0.114 ms
21:22:12.632:      ┣OBSBasic::ResetAudio: 0.36 ms
21:22:12.632:      ┣OBSBasic::ResetVideo: 116.221 ms
21:22:12.632:      ┃ ┗obs_init_graphics: 110.105 ms
21:22:12.632:      ┃   ┗shader compilation: 32.834 ms
21:22:12.632:      ┣OBSBasic::InitOBSCallbacks: 0.008 ms
21:22:12.632:      ┣OBSBasic::InitHotkeys: 0.051 ms
21:22:12.632:      ┣obs_load_all_modules2: 434.207 ms
21:22:12.632:      ┃ ┣obs_init_module(decklink-captions.so): 0.002 ms
21:22:12.632:      ┃ ┣obs_init_module(decklink-output-ui.so): 0.001 ms
21:22:12.632:      ┃ ┣obs_init_module(decklink.so): 0.174 ms
21:22:12.632:      ┃ ┣obs_init_module(frontend-tools.so): 83.325 ms
21:22:12.632:      ┃ ┣obs_init_module(image-source.so): 0.012 ms
21:22:12.632:      ┃ ┣obs_init_module(linux-alsa.so): 0.006 ms
21:22:12.632:      ┃ ┣obs_init_module(linux-capture.so): 0.002 ms
21:22:12.632:      ┃ ┣obs_init_module(linux-jack.so): 0.005 ms
21:22:12.632:      ┃ ┣obs_init_module(linux-pipewire.so): 19.755 ms
21:22:12.632:      ┃ ┣obs_init_module(linux-pulseaudio.so): 0.009 ms
21:22:12.632:      ┃ ┣obs_init_module(linux-v4l2.so): 5.571 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-ffmpeg.so): 1.566 ms
21:22:12.632:      ┃ ┃ ┗nvenc_check: 0.832 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-filters.so): 0.037 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-libfdk.so): 0.004 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-outputs.so): 0.005 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-qsv11.so): 0.169 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-transitions.so): 0.011 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-vst.so): 0.01 ms
21:22:12.632:      ┃ ┣obs_init_module(obs-x264.so): 0.005 ms
21:22:12.632:      ┃ ┣obs_init_module(rtmp-services.so): 1.307 ms
21:22:12.632:      ┃ ┣obs_init_module(text-freetype2.so): 0.024 ms
21:22:12.632:      ┃ ┗obs_init_module(wlrobs.so): 0.032 ms
21:22:12.632:      ┣OBSBasic::InitService: 1.531 ms
21:22:12.633:      ┣OBSBasic::ResetOutputs: 0.168 ms
21:22:12.633:      ┣OBSBasic::CreateHotkeys: 0.023 ms
21:22:12.633:      ┣OBSBasic::InitPrimitives: 0.072 ms
21:22:12.633:      ┗OBSBasic::Load: 32.738 ms
21:22:12.633: obs_hotkey_thread(25 ms): min=0.001 ms, median=0.002 ms, max=0.048 ms, 99th percentile=0.004 ms, 100% below 25 ms
21:22:12.633: audio_thread(Audio): min=0.019 ms, median=0.071 ms, max=29.752 ms, 99th percentile=14.6 ms
21:22:12.633: obs_graphics_thread(16.6667 ms): min=0.274 ms, median=1.276 ms, max=53.633 ms, 99th percentile=18.718 ms, 96.8758% below 16.667 ms
21:22:12.633:  ┣tick_sources: min=0.001 ms, median=0.013 ms, max=41.21 ms, 99th percentile=0.036 ms
21:22:12.633:  ┣output_frame: min=0.132 ms, median=0.432 ms, max=33.993 ms, 99th percentile=18.478 ms
21:22:12.633:  ┃ ┗gs_context(video->graphics): min=0.129 ms, median=0.428 ms, max=33.99 ms, 99th percentile=18.476 ms
21:22:12.633:  ┃   ┣render_video: min=0.013 ms, median=0.09 ms, max=33.569 ms, 99th percentile=18.342 ms
21:22:12.633:  ┃   ┃ ┗render_main_texture: min=0.01 ms, median=0.083 ms, max=33.563 ms, 99th percentile=18.337 ms
21:22:12.633:  ┃   ┗gs_flush: min=0.002 ms, median=0.299 ms, max=0.997 ms, 99th percentile=0.687 ms
21:22:12.633:  ┗render_displays: min=0.005 ms, median=0.73 ms, max=15.759 ms, 99th percentile=1.523 ms
21:22:12.633: =================================================
21:22:12.633: == Profiler Time Between Calls ==================
21:22:12.633: obs_hotkey_thread(25 ms): min=2.337 ms, median=25.112 ms, max=25.792 ms, 99.6503% within ±2% of 25 ms (0.03885% lower, 0.3108% higher)
21:22:12.633: obs_graphics_thread(16.6667 ms): min=8.51 ms, median=16.667 ms, max=53.65 ms, 93.151% within ±2% of 16.667 ms (2.60417% lower, 4.24479% higher)
21:22:12.633: =================================================
21:22:12.640: Number of memory leaks: 0
