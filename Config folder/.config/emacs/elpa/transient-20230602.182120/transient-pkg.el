(define-package "transient" "20230602.182120" "Transient commands"
  '((emacs "25.1")
    (compat "29.1.4.1"))
  :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("extensions")
  :url "https://github.com/magit/transient")
;; Local Variables:
;; no-byte-compile: t
;; End:
