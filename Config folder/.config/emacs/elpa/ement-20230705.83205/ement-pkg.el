(define-package "ement" "20230705.83205" "Matrix client"
  '((emacs "27.1")
    (map "2.1")
    (persist "0.5")
    (plz "0.6")
    (taxy "0.10")
    (taxy-magit-section "0.12.1")
    (svg-lib "0.2.5")
    (transient "0.3.7"))
  :authors
  '(("Adam Porter" . "adam@alphapapa.net"))
  :maintainer
  '("Adam Porter" . "adam@alphapapa.net")
  :keywords
  '("comm")
  :url "https://github.com/alphapapa/ement.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
